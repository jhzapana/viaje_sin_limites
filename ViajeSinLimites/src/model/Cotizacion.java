package model;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Cotizacion {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	@Unowned
	private Proveedor proveedor;
	
	
	@Persistent
	@Unowned
	private Cliente cliente;
	
	
	@Persistent
	private double costoTotal;
	
	@Persistent
	private double costoUnitario;
	
	@Persistent
	private int numeroPersonas;
	

	@Persistent
	private String descripcion;
	
	@Persistent
	private String fecha;
	
	@Persistent
	private String estReg;


	public Cotizacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cotizacion(double costoTotal, double costoUnitario,
			int numeroPersonas, String descripcion, String fecha) {
		super();
		this.costoTotal = costoTotal;
		this.costoUnitario = costoUnitario;
		this.numeroPersonas = numeroPersonas;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.estReg = "A";
	}
	
	public String getId() {
		return KeyFactory.keyToString(id);
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public double getCostoTotal() {
		return costoTotal;
	}

	public void setCostoTotal(double costoTotal) {
		this.costoTotal = costoTotal;
	}

	public double getCostoUnitario() {
		return costoUnitario;
	}

	public void setCostoUnitario(double costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public int getNumeroPersonas() {
		return numeroPersonas;
	}

	public void setNumeroPersonas(int numeroPersonas) {
		this.numeroPersonas = numeroPersonas;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getEstReg() {
		return estReg;
	}

	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}
	
	
	
	
	
	
	

	
}
