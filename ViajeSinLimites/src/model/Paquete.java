package model;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

import model.Cliente;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Paquete {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String fecha;
	

	@Persistent
	@Unowned
	private Administrador administrador;
	
	@Persistent
	private String ciudadOrigen;
	
	@Persistent
	private String ciudadDestino;
	
	@Persistent
	private double costo;
	
	@Persistent
	@Unowned
	private List<Proveedor> proveedores = new ArrayList<Proveedor>();
	
	@Persistent
	@Unowned
	private List<PaqueteDet> detalle = new ArrayList<PaqueteDet>();
	
	@Persistent
	private int dias;
	
	@Persistent
	private int noches;
	
	@Persistent
	private String estReg;
	

	public Paquete() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Paquete(String nombre, String descripcion, String fecha,String origen,String destino,
			double costo, int dias, int noches) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.costo = costo;
		this.dias = dias;
		this.noches = noches;
		this.ciudadOrigen = origen;
		this.ciudadDestino = destino;
		this.estReg = "A";
	}

	public String getId() {
		return KeyFactory.keyToString(id);
	}
	

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public Administrador getAdministrador() {
		return administrador;
	}


	public void setAdministrador(Administrador administrador) {
		this.administrador = administrador;
	}


	





	public String getCiudadOrigen() {
		return ciudadOrigen;
	}


	public void setCiudadOrigen(String ciudadOrigen) {
		this.ciudadOrigen = ciudadOrigen;
	}


	public String getCiudadDestino() {
		return ciudadDestino;
	}


	public void setCiudadDestino(String ciudadDestino) {
		this.ciudadDestino = ciudadDestino;
	}


	public double getCosto() {
		return costo;
	}


	public void setCosto(double costo) {
		this.costo = costo;
	}


	public List<Proveedor> getProveedores() {
		return proveedores;
	}


	public void setProveedores(List<Proveedor> proveedores) {
		this.proveedores = proveedores;
	}


	public String getEstReg() {
		return estReg;
	}


	public void setEstReg(String estReg) {
		this.estReg = estReg;
	}


	public List<PaqueteDet> getDetalle() {
		return detalle;
	}


	public void setDetalle(List<PaqueteDet> detalle) {
		this.detalle = detalle;
	}


	public int getDias() {
		return dias;
	}


	public void setDias(int dias) {
		this.dias = dias;
	}


	public int getNoches() {
		return noches;
	}


	public void setNoches(int noches) {
		this.noches = noches;
	}
	
	
	
	
	
	
	

}
