package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class PaqueteCabecera{
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String fecha;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String costo;
	
	@Persistent
	private String origen;
	
	@Persistent
	private String destino;
	
	@Persistent
	private String creador;
	
	@Persistent
	private String estReg;
	public PaqueteCabecera(String nombre, String fecha, String descripcion,String costo,String origen,String destino,String creador){
		
		super();
		this.nombre =nombre;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.costo = costo;
		this.origen = origen;
		this.destino= destino;
		this.creador = creador;
		
		this.estReg = "A";
		
	}
	public PaqueteCabecera(){
		super();
	}
	public String getId() {
		return KeyFactory.keyToString(id);
	}

	public void setId(String idcabe) {
		Key keyCabecera = KeyFactory.stringToKey(idcabe);
		this.id = KeyFactory.createKey(keyCabecera,
		PaqueteDetalle.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
		
	}
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha){
		this.fecha = fecha;
		
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
		
	}
	
	
	public String getCosto() {
		return costo;
	}
	public void setCosto(String costo){
		this.costo = costo;
		
	}
	
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen){
		this.origen = origen;
		
	}
	
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino){
		this.destino = destino;
		
	}
	
	public String getCreador() {
		return creador;
	}
	public void setCreador(String creador){
		this.creador = creador;
		
	}
	
	
	
	
}
