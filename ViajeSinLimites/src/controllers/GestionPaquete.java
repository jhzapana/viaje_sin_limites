package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;

@SuppressWarnings("serial")
public class GestionPaquete extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(PaqueteCabecera.class);
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean status = false;
		
		
		String estReg = "A";
		//q.setOrdering("id ascending");
		q.setOrdering("id descending");
		//q.setRange(0, 10);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		try{
			
			@SuppressWarnings("unchecked")
			List<PaqueteCabecera> cabeceras = (List<PaqueteCabecera>) q.execute(estReg);
			//req.setAttribute("personas", cabeceras);
			//RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listpersona01.jsp");
			//rd.forward(req, resp);
			
				status = true;
			map.put("status", status);
			map.put("numero",cabeceras.size());
			System.out.println(cabeceras.size());
		
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
		write(resp,map);
		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}
	

}
