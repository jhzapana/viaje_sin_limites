package controllers;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Cliente;
import model.PMF;
import model.PaqueteCabecera;

@SuppressWarnings("serial")
public class Inicio extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/plain");
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(PaqueteCabecera.class);
		
		String estReg = "A";
		//q.setOrdering("id ascending");
		q.setOrdering("id descending");
		//q.setRange(0, 10);
		q.setFilter("estReg == estRegParam");
		q.declareParameters("String estRegParam");
		
		try{
			
			@SuppressWarnings("unchecked")
			List<PaqueteCabecera> cabeceras = (List<PaqueteCabecera>) q.execute(estReg);
			//req.setAttribute("personas", cabeceras);
			//RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/listpersona01.jsp");
			//rd.forward(req, resp);
			
			getServletContext().setAttribute("cabeceras",cabeceras);
			System.out.println(cabeceras.size());
		
		}catch(Exception e){
			System.out.println(e);
		}finally{
			q.closeAll();
			pm.close();
		}
			if((getServletContext().getAttribute("cliente"))==null){
				Cliente nuevo = new Cliente();
				getServletContext().setAttribute("cliente",nuevo);
			}
			if((getServletContext().getAttribute("exitoDetalle"))==null){
				Boolean exito = false;
				getServletContext().setAttribute("exitoDetalle",exito);
			}
	
			HttpSession misesion= req.getSession();
			System.out.println(misesion.getAttribute("username"));
			if(misesion.getValue("username") != null)
				System.out.println("haysesion");
			else
				System.out.println("no hay sesion");
			resp.sendRedirect("inicio.jsp");
		
		
	}
	

}
