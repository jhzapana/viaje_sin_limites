package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;

@SuppressWarnings("serial")
public class CrearPaquete extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		String costo = req.getParameter("costo");
		String origen = req.getParameter("origen");
		String destino = req.getParameter("destino");
		String fecha = req.getParameter("fecha");
		String creador = req.getParameter("creador");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final PaqueteCabecera nuevopaquete =  new PaqueteCabecera(nombre,fecha,descripcion,costo,origen,destino,creador);
		
		
		try{
			pm.makePersistent(nuevopaquete);
		
			exito = true;
			map.put("paquete", nuevopaquete.getNombre());
			map.put("exito", exito);
			
			
			
		}catch(Exception e){
			System.out.println(e);
			//String mensaje_error = "Ocurrio un error";
		
			
		}finally{
			pm.close();
		}
	
		write(resp,map);
		

		
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
