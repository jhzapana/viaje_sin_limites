<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import = "model.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>


<% HttpSession misesion= request.getSession(); %>
<%
//if( (getServletContext().getAttribute("votantes")) != null ){
	Cliente cliente = (Cliente) getServletContext().getAttribute("cliente");
//}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Inicio Cliente</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link rel="stylesheet" href="css/ed-grid.css">
	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/estilo_cliente.css">
	
</head>
<body>
	<div class="grupo total">
			<div class="caja movil-100 cabecera">

				<header>
					<div class="menu_bar">
						<a href="#" class="bt-menu"><span class="icon-th-menu">menu</span>Menu</a>
					</div>
 
					<nav id = "nav1">
						<ul>
							<li><a href="inicio.jsp"><span class="icon-home3"></span>Inicio</a></li>
							<li><a href="#">Cotizaciones</a></li>
							<li class="submenu">
								<a href="#">Viajes  a Medida<span class="caret"></span></a>
								<ul class="children">
									<li><a href="#">Encontrar Viaje <span class="icon-zoom"></span></a></li>
									<li><a href="#">Crear Nuevo <span class="icon-edit"></span></a></li>
									
								</ul>
							</li>
							
							<li><a href="#">Paquetes</a></li>
							<li><a href="#">Nosotros</a></li>
							
						
						</ul>
					</nav>
				</header>
				
			</div>
		</div>
		<% if (misesion.getValue("username") != null){%>
		
		
		<div class="grupo tabla">
			<div class="caja movil-100" id="repuestaCliente">
				
			</div>
			<div class="caja movil-30 cajaA">
			<center>
				<button class="button bt" id = "btn_modificar"><center>Modificar datos</center></button>
				<a href="" class="button bt">Calendario</a>
				<a href="" class="button bt" id = "mispaquetes">Mis Paquetes</a>
				<a href="" class="button bt" id = "a">Paquetes</a>
				
				
			</center>
			</div>
			<div class="caja movil-70 cajaB" id = "idcajaB">
				
				<div id="modificardiv" >
					<form action="modificarCliente" method= "post" id="form_modificar">
						<div id = "div_form">
							<label for ="id">Id:</label>
							<input type="text" name = "id"  value= "<%= cliente.getId() %>" readonly="readonly"><br>
						</div>
						<div id = "div_form">
							<label for ="nombres">Nombres:</label>
							<input type="text" name = "nombres"  value= "<%= cliente.getNombres() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="apellidoP">Apellido Paterno:</label>
							<input type="text" name = "apellidoP"  value= "<%= cliente.getApellidoP() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="apellidoM">Apellido Materno:</label>
							<input type="text" name = "apellidoM"  value= "<%= cliente.getApellidoM() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="telefono">Telefono:</label>
							<input type="text" name = "telefono"  value= "<%= cliente.getTelefono() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="correo">Correo:</label>
							<input type="text" name = "correo"  value= "<%= cliente.getCorreo() %>"><br>
						</div>
						<div id = "div_form">
							<label for ="username">Username:</label>
							<input type="text" name = "username"  value= "<%= cliente.getUsername() %>"  readonly="readonly "><br>
						</div>
						<div id = "div_form">
							<label for ="password">Password:</label>
							<input type="text" name = "password""  value= "<%= cliente.getPassword() %>"><br><br>
						</div>
						<br><br>
						
						<center><input type="submit" value= "Modificar" id = "btn_submit_modificar" class = "button"></center>
					</form>
				</div>
				<div id="div_mispaquetes">
				dff
						
				</div>
			</div>
			
		
		</div>
		<%}%>
		
		
		
		<script src="js/jquery-1.12.4.min.js"></script>
		
		<script src="js/main.js"></script>

</body>
</html>